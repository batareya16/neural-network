using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;

namespace CodingBackProp
{
    public static class ImageParser
    {
        const int Size = 20;
        private static readonly string[] Letters =
            "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z"
                .Split(" ");

        public static string DataToLetter(double[] data)
        {
            var allLetters =
                Letters.Concat(Letters.Select(x => x.ToLower())).ToArray();
            var maxValue = data.Max();
            var maxIndex = Array.IndexOf(data, maxValue);

            return allLetters[maxIndex];
        }

        public static double[] ParseImage(string inputPath, double[] expectedResult = null)
        {
            using var image = new Bitmap(Image.FromFile(inputPath));

            var resized = new Bitmap(Size, Size);
            using (var graphics = Graphics.FromImage(resized))
            {
                graphics.CompositingQuality = CompositingQuality.HighSpeed;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.DrawImage(image, 0, 0, Size, Size);
            }

            var colors = new double[Size * Size + (expectedResult?.Length ?? 0)];
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    colors[i * Size + j] = resized.GetPixel(i, j).R +
                                           resized.GetPixel(i, j).G +
                                           resized.GetPixel(i, j).B > 500 ? 1 : 0;
                }
            }

            for (var i = 0; i < (expectedResult?.Length ?? 0); i++)
            {
                colors[Size * Size + i] = expectedResult[i];
            }

            return colors;
        }

        public static double[][] ParseImages(out int filesCount)
        {
            var filesInfo = GetFilesInfo();
            var result = new double[filesInfo.Length][];
            for (int i = 0; i < filesInfo.Length; i++)
            {
                if (i % 500 == 0) ConsoleHelper.DrawTextProgressBar(i, filesInfo.Length);
                result[i] = ParseImage(filesInfo[i].file, filesInfo[i].expectedResult);
            }

            filesCount = filesInfo.Length;
            return result;
        }

        private static (string file, double[] expectedResult)[] GetFilesInfo()
        {
            var commonPath = $"{Program.HomePath}/training";
            var result = new List<(string file, double[] expectedResult)>();
            for (var i = 0; i < Letters.Length; i++)
            {
                var outputExpected = new double[Letters.Length * 2];
                outputExpected[i] = 1;

                result
                    .AddRange(Directory.GetFiles($"{commonPath}/upper/{Letters[i]}", "*.gif")
                        .Select(file => (file, outputExpected)));
            }

            for (var i = 0; i < Letters.Length; i++)
            {
                var outputExpected = new double[Letters.Length * 2];
                outputExpected[Letters.Length + i] = 1;
                result
                    .AddRange(Directory.GetFiles($"{commonPath}/lower/{Letters[i]}", "*.gif")
                        .Select(file => (file, outputExpected)));
            }

            return result.ToArray();
        }
    }
}
