using System;

namespace CodingBackProp
{
    public class NeuralNetwork
    {
        private readonly int _numInput;
        private readonly int _numHidden;
        private readonly int _numOutput;

        private readonly double[] _inputs;
        private readonly double[][] _ihWeights;
        private readonly double[] _hBiases;
        private readonly double[] _hOutputs;

        private readonly double[][] _hoWeights;
        private readonly double[] _oBiases;
        private readonly double[] _outputs;

        private readonly Random _rnd;

        public NeuralNetwork(int numInput, int numHidden, int numOutput)
        {
            _numInput = numInput;
            _numHidden = numHidden;
            _numOutput = numOutput;

            _inputs = new double[numInput];

            _ihWeights = MakeMatrix(numInput, numHidden, 0.0);
            _hBiases = new double[numHidden];
            _hOutputs = new double[numHidden];

            _hoWeights = MakeMatrix(numHidden, numOutput, 0.0);
            _oBiases = new double[numOutput];
            _outputs = new double[numOutput];

            _rnd = new Random(0);
            InitializeWeights();
        }

        private static double[][] MakeMatrix(int rows,
            int cols, double v) // helper for ctor, Train
        {
            var result = new double[rows][];
            for (var r = 0; r < result.Length; ++r)
            {
                result[r] = new double[cols];
            }

            for (var i = 0; i < rows; ++i)
            for (var j = 0; j < cols; ++j)
            {
                result[i][j] = v;
            }

            return result;
        }

        private void InitializeWeights()
        {
            var numWeights = _numInput * _numHidden +
                             _numHidden * _numOutput + _numHidden + _numOutput;

            var initialWeights = new double[numWeights];
            for (var i = 0; i < initialWeights.Length; ++i)
            {
                initialWeights[i] = (0.001 - 0.0001) * _rnd.NextDouble() + 0.0001;
            }

            SetWeights(initialWeights);
        }

        public void SetWeights(double[] weights)
        {
            var numWeights = _numInput * _numHidden +
                             _numHidden * _numOutput + _numHidden + _numOutput;

            if (weights.Length != numWeights)
                throw new Exception("Bad weights array in SetWeights");

            var k = 0;

            for (var i = 0; i < _numInput; ++i)
            for (var j = 0; j < _numHidden; ++j)
            {
                _ihWeights[i][j] = weights[k++];
            }

            for (var i = 0; i < _numHidden; ++i)
            {
                _hBiases[i] = weights[k++];
            }

            for (var i = 0; i < _numHidden; ++i)
            for (var j = 0; j < _numOutput; ++j)
            {
                _hoWeights[i][j] = weights[k++];
            }

            for (var i = 0; i < _numOutput; ++i)
            {
                _oBiases[i] = weights[k++];
            }
        }

        private double[] GetWeights()
        {
            var numWeights = _numInput * _numHidden +
                             _numHidden * _numOutput + _numHidden + _numOutput;

            var result = new double[numWeights];
            var k = 0;
            foreach (var t in _ihWeights)
                for (var j = 0; j < _ihWeights[0].Length; ++j)
                    result[k++] = t[j];

            foreach (var t in _hBiases)
                result[k++] = t;

            foreach (var t in _hoWeights)
                for (var j = 0; j < _hoWeights[0].Length; ++j)
                {
                    result[k++] = t[j];
                }

            foreach (var t in _oBiases)
                result[k++] = t;

            return result;
        }

        public double[] ComputeOutputs(double[] xValues)
        {
            double[] hSums = new double[_numHidden];
            double[] oSums = new double[_numOutput];

            for (int i = 0; i < xValues.Length; ++i) // copy x-values to inputs
                _inputs[i] = xValues[i];
            // note: no need to copy x-values unless you implement a ToString.
            // more efficient is to simply use the xValues[] directly.

            for (int j = 0; j < _numHidden; ++j)  // compute i-h sum of weights * inputs
            for (int i = 0; i < _numInput; ++i)
                hSums[j] += _inputs[i] * _ihWeights[i][j]; // note +=

            for (int i = 0; i < _numHidden; ++i)  // add biases to hidden sums
                hSums[i] += _hBiases[i];

            for (int i = 0; i < _numHidden; ++i)   // apply activation
                this._hOutputs[i] = HyperTan(hSums[i]); // hard-coded

            for (int j = 0; j < _numOutput; ++j)   // compute h-o sum of weights * hOutputs
            for (int i = 0; i < _numHidden; ++i)
                oSums[j] += _hOutputs[i] * _hoWeights[i][j];

            for (int i = 0; i < _numOutput; ++i)  // add biases to output sums
                oSums[i] += _oBiases[i];

            double[] softOut = Softmax(oSums); // all outputs at once for efficiency
            Array.Copy(softOut, _outputs, softOut.Length);

            double[] retResult = new double[_numOutput]; // could define a GetOutputs
            Array.Copy(_outputs, retResult, retResult.Length);
            return retResult;
        }

        private static double HyperTan(double x)
        {
            if (x < -20.0) return -1.0;
            if (x > 20.0) return 1.0;
            return Math.Tanh(x);
        }

        private static double[] Softmax(double[] oSums)
        {
            // does all output nodes at once so scale
            // doesn't have to be re-computed each time

            double sum = 0.0;
            for (int i = 0; i < oSums.Length; ++i)
                sum += Math.Exp(oSums[i]);

            double[] result = new double[oSums.Length];
            for (int i = 0; i < oSums.Length; ++i)
                result[i] = Math.Exp(oSums[i]) / sum;

            return result; // now scaled so that xi sum to 1.0
        }

        public double[] Train(double[][] trainData, int maxEpochs,
            double learnRate, double momentum)
        {
            // train using back-prop
            // back-prop specific arrays
            double[][] hoGrads = MakeMatrix(_numHidden, _numOutput, 0.0); // hidden-to-output weight gradients
            double[] obGrads = new double[_numOutput];                   // output bias gradients

            double[][] ihGrads = MakeMatrix(_numInput, _numHidden, 0.0);  // input-to-hidden weight gradients
            double[] hbGrads = new double[_numHidden];                   // hidden bias gradients

            double[] oSignals = new double[_numOutput];                  // local gradient output signals - gradients w/o associated input terms
            double[] hSignals = new double[_numHidden];                  // local gradient hidden node signals

            // back-prop momentum specific arrays
            double[][] ihPrevWeightsDelta = MakeMatrix(_numInput, _numHidden, 0.0);
            double[] hPrevBiasesDelta = new double[_numHidden];
            double[][] hoPrevWeightsDelta = MakeMatrix(_numHidden, _numOutput, 0.0);
            double[] oPrevBiasesDelta = new double[_numOutput];

            var epoch = 0;
            var xValues = new double[_numInput]; // inputs
            var tValues = new double[_numOutput]; // target values
            double derivative;
            double errorSignal;

            int[] sequence = new int[trainData.Length];
            for (int i = 0; i < sequence.Length; ++i)
                sequence[i] = i;


            while (epoch < maxEpochs)
            {
                ++epoch;

                double trainErr = Error(trainData);
                Console.WriteLine($"epoch = {epoch}  error = {trainErr:F4}");

                Shuffle(sequence); // visit each training data in random order
                for (int ii = 0; ii < trainData.Length; ++ii)
                {
                    int idx = sequence[ii];
                    Array.Copy(trainData[idx], xValues, _numInput);
                    Array.Copy(trainData[idx],_numInput, tValues, 0, _numOutput);
                    ComputeOutputs(xValues); // copy xValues in, compute outputs

                    // i = inputs, j = hiddens, k = outputs
                    // 1. compute output node signals (assumes softmax)
                    for (int k = 0; k < _numOutput; ++k)
                    {
                        errorSignal = tValues[k] - _outputs[k];  // Wikipedia uses (o-t)
                        derivative = (1 - _outputs[k]) * _outputs[k]; // for softmax
                        oSignals[k] = errorSignal * derivative;
                    }

                    // 2. compute hidden-to-output weight gradients using output signals
                    for (int j = 0; j < _numHidden; ++j)
                    for (int k = 0; k < _numOutput; ++k)
                        hoGrads[j][k] = oSignals[k] * _hOutputs[j];

                    // 2b. compute output bias gradients using output signals
                    for (int k = 0; k < _numOutput; ++k)
                        obGrads[k] = oSignals[k] * 1.0; // dummy assoc. input value

                    // 3. compute hidden node signals
                    for (int j = 0; j < _numHidden; ++j)
                    {
                        derivative = (1 + _hOutputs[j]) * (1 - _hOutputs[j]); // for tanh
                        double sum = 0.0; // need sums of output signals times hidden-to-output weights
                        for (int k = 0; k < _numOutput; ++k) {
                            sum += oSignals[k] * _hoWeights[j][k]; // represents error signal
                        }
                        hSignals[j] = derivative * sum;
                    }

                    // 4. compute input-hidden weight gradients
                    for (int i = 0; i < _numInput; ++i)
                    for (int j = 0; j < _numHidden; ++j)
                        ihGrads[i][j] = hSignals[j] * _inputs[i];

                    // 4b. compute hidden node bias gradients
                    for (int j = 0; j < _numHidden; ++j)
                        hbGrads[j] = hSignals[j] * 1.0; // dummy 1.0 input

                    // == update weights and biases

                    // update input-to-hidden weights
                    for (int i = 0; i < _numInput; ++i)
                    {
                        for (int j = 0; j < _numHidden; ++j)
                        {
                            double delta = ihGrads[i][j] * learnRate;
                            _ihWeights[i][j] += delta; // would be -= if (o-t)
                            _ihWeights[i][j] += ihPrevWeightsDelta[i][j] * momentum;
                            ihPrevWeightsDelta[i][j] = delta; // save for next time
                        }
                    }

                    // update hidden biases
                    for (int j = 0; j < _numHidden; ++j)
                    {
                        double delta = hbGrads[j] * learnRate;
                        _hBiases[j] += delta;
                        _hBiases[j] += hPrevBiasesDelta[j] * momentum;
                        hPrevBiasesDelta[j] = delta;
                    }

                    // update hidden-to-output weights
                    for (int j = 0; j < _numHidden; ++j)
                    {
                        for (int k = 0; k < _numOutput; ++k)
                        {
                            double delta = hoGrads[j][k] * learnRate;
                            _hoWeights[j][k] += delta;
                            _hoWeights[j][k] += hoPrevWeightsDelta[j][k] * momentum;
                            hoPrevWeightsDelta[j][k] = delta;
                        }
                    }

                    // update output node biases
                    for (int k = 0; k < _numOutput; ++k)
                    {
                        double delta = obGrads[k] * learnRate;
                        _oBiases[k] += delta;
                        _oBiases[k] += oPrevBiasesDelta[k] * momentum;
                        oPrevBiasesDelta[k] = delta;
                    }

                }

            }
            double[] bestWts = GetWeights();
            return bestWts;
        }

        private void Shuffle(int[] sequence) // instance method
        {
            for (int i = 0; i < sequence.Length; ++i)
            {
                int r = _rnd.Next(i, sequence.Length);
                int tmp = sequence[r];
                sequence[r] = sequence[i];
                sequence[i] = tmp;
            }
        }

        private double Error(double[][] trainData)
        {
            // average squared error per training item
            double sumSquaredError = 0.0;
            double[] xValues = new double[_numInput]; // first numInput values in trainData
            double[] tValues = new double[_numOutput]; // last numOutput values

            // walk thru each training case. looks like (6.9 3.2 5.7 2.3) (0 0 1)
            foreach (var t in trainData)
            {
                Array.Copy(t, xValues, _numInput);
                Array.Copy(t, _numInput, tValues, 0, _numOutput); // get target values
                var yValues = this.ComputeOutputs(xValues); // outputs using current weights
                for (var j = 0; j < _numOutput; ++j)
                {
                    double err = tValues[j] - yValues[j];
                    sumSquaredError += err * err;
                }
            }
            return sumSquaredError / trainData.Length;
        } // MeanSquaredError

        public double Accuracy(double[][] testData)
        {
            // percentage correct using winner-takes all
            int numCorrect = 0;
            int numWrong = 0;
            double[] xValues = new double[_numInput]; // inputs
            double[] tValues = new double[_numOutput]; // targets
            double[] yValues; // computed Y

            foreach (var t in testData)
            {
                Array.Copy(t, xValues, _numInput); // get x-values
                Array.Copy(t, _numInput, tValues, 0, _numOutput); // get t-values
                yValues = ComputeOutputs(xValues);
                int maxIndex = MaxIndex(yValues); // which cell in yValues has largest value?
                int tMaxIndex = MaxIndex(tValues);

                if (maxIndex == tMaxIndex)
                    ++numCorrect;
                else
                    ++numWrong;
            }
            return (numCorrect * 1.0) / (numCorrect + numWrong);
        }

        private static int MaxIndex(double[] vector) // helper for Accuracy()
        {
            // index of largest value
            int bigIndex = 0;
            double biggestVal = vector[0];
            for (int i = 0; i < vector.Length; ++i)
            {
                if (vector[i] > biggestVal)
                {
                    biggestVal = vector[i];
                    bigIndex = i;
                }
            }
            return bigIndex;
        }
    }
}
