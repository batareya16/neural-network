﻿using System;
using System.IO;
using System.Linq;

namespace CodingBackProp
{
    static class Program
    {
        public const string HomePath = "/Users/batareya16/RiderProjects/NeuralNetwork/NeuralNetwork/bin/Debug/netcoreapp3.1";

        static void Main()
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Clear();
            Console.WriteLine("Loading images....");
            var numInput = 20 * 20;
            var numOutput = 26 + 26;
            var numHidden = (int) (numInput * (2.0f / 3));
            var allData = ImageParser.ParseImages(out _);
            var seed = 1;
            Console.WriteLine("Done! Images are loaded!");
            DataHelper.SplitTrainTest(allData, 0.80, seed, out var trainData, out var testData);
            Console.WriteLine("Data has been prepared!");
            Console.WriteLine("Creating a " + numInput + "-" + numHidden +
                              "-" + numOutput + " neural network");
            var neuralNetwork = new NeuralNetwork(numInput, numHidden, numOutput);

            var learnRate = 0.05;
            var momentum = 0.01;
            Console.WriteLine("Learn rate = " + learnRate.ToString("F2"));
            Console.WriteLine("Momentum  = " + momentum.ToString("F2"));

            while (true)
            {
                Console.WriteLine("\nPress 1 to train 5 epochs");
                Console.WriteLine("Press 2 to show info");
                Console.WriteLine("Press 3 to predict image");
                Console.WriteLine("Press 4 to load saved network");
                Console.WriteLine("Press 5 to exit");
                Console.WriteLine("\nPress any key to continue...");

                var input = Console.ReadKey();
                switch (input.Key)
                {
                    case ConsoleKey.D1:
                        Console.WriteLine("\nStarting training...");
                        neuralNetwork.Train(trainData, 5, learnRate, momentum);
                        Console.WriteLine("Done");
                        break;
                    case ConsoleKey.D2:
                        var trainAcc = neuralNetwork.Accuracy(trainData);
                        Console.WriteLine("\nFinal accuracy on training data = " + trainAcc.ToString("F4"));

                        var testAcc = neuralNetwork.Accuracy(testData);
                        Console.WriteLine("Final accuracy on test data     = " + testAcc.ToString("F4"));
                        break;
                    case ConsoleKey.D3:
                        Console.WriteLine("Type file name:\n");
                        var fileName = Console.ReadLine();
                        try
                        {
                            var inputValues = ImageParser.ParseImage(
                                $"{HomePath}/{fileName}");
                            var outputs = neuralNetwork.ComputeOutputs(inputValues);
                            Console.WriteLine($"\nPredicted result: {ImageParser.DataToLetter(outputs)}");
                        }
                        catch
                        {
                            Console.WriteLine("Error loading file");
                        }

                        break;
                    case ConsoleKey.D4:
                        RestoreNetworkAction(ref neuralNetwork);
                        break;
                    case ConsoleKey.D5:
                        Console.WriteLine("Saving network...");
                        Serializer.WriteToBinaryFile($"{HomePath}/saves/{DateTime.Now.Ticks}.nn", neuralNetwork);
                        Environment.Exit(0);
                        break;
                }
            }
        }

        private static void RestoreNetworkAction(ref NeuralNetwork neuralNetwork)
        {
            Console.WriteLine("Getting networks list...");
            var files = Directory.GetFiles($"{HomePath}/saves").ToList();
            Console.WriteLine($"Found {files.Count} files. Type key to load:");
            Console.WriteLine("0 - Cancel. Do not load");
            for (int i = 0; i < files.Count; i++)
            {
                Console.WriteLine($"{i + 1} - {files[i]}");
            }

            Console.WriteLine("");
            var line = Console.ReadLine();
            var isSuccess = int.TryParse(line, out int result);
            if (!isSuccess || result <= 0 || result >= files.Count)
            {
                return;
            }

            neuralNetwork = Serializer.ReadFromBinaryFile<NeuralNetwork>(files[result]);
        }
    }
}
